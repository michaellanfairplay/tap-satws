"""SatWS tap class."""
from datetime import datetime
from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
from tap_satws.streams import InvoiceStream

STREAM_TYPES = [
    InvoiceStream,
]


class TapSatWS(Tap):
    """SatWS tap class."""
    name = "tap-satws"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The api key to authenticate against the API service",
        ),
        th.Property(
            "client_id",
            th.StringType,
            required=True,
            description="The RFC code",
        ),
        th.Property(
            "company_id",
            th.StringType,
            required=True,
            description="Company ID",
        ),
        th.Property(
            "connector_id",
            th.StringType,
            required=True,
            description="Connector ID",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            required=True,
            description="Filter by creation date (greater than or equal >=)",
        ),
        th.Property(
            "end_date",
            th.DateTimeType,
            default=f"{datetime.now().replace(hour=0, minute=0, second=0, microsecond=0).isoformat(timespec='milliseconds')}Z",
            description="Filter by creation date (less than or equal <=)",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
