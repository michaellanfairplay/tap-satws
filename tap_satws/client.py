"""REST client handling, including SatWSStream base class."""
from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator


class SatWSStream(RESTStream):
    """SatWS stream class."""

    # url_base = "https://api.sandbox.satws.com"
    url_base = "https://api.satws.com"

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self,
            key="X-API-Key",
            value=self.config.get("api_key"),
            location="header"
        )
