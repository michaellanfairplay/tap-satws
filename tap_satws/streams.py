"""Stream type classes for tap-satws."""

import requests
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Optional, Iterable
from urllib.parse import urlparse, parse_qsl

from tap_satws.client import SatWSStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class InvoiceStream(SatWSStream):
    """Define custom stream."""

    STATE_MSG_FREQUENCY = 20
    name = "invoices"
    primary_keys = ["id"]
    path = "/taxpayers/{client_id}/invoices"
    replication_key = "issuedAt"

    schema_filepath = SCHEMAS_DIR / "invoices.json"

    def get_starting_created_value(self, context: Optional[dict]) -> str:
        context_state = self.get_context_state(context)
        state_date = context_state.get('replication_key_value', None)
        start_date = context_state.get('starting_replication_value')

        if isinstance(state_date, str):
            state_date = datetime.strptime(state_date, '%Y-%m-%d %H:%M:%S').\
                replace(hour=0, minute=0, second=0, microsecond=0).\
                isoformat(timespec='milliseconds')
            return f"{state_date}Z"

        return start_date

    def get_next_page_token(
            self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        next_page = response.json()['hydra:view'].get('hydra:next', None)
        next_page_token = dict(parse_qsl(urlparse(next_page).query)).get('page', None) if next_page else None
        return next_page_token

    def get_url_params(
            self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        start_date = self.get_starting_created_value(context)
        params: dict = {
            "order[issuedAt]": "asc",
            "issuedAt[after]": start_date,
            "itemsPerPage": 100,
        }
        if next_page_token:
            params["page"] = next_page_token
        if self.config.get('end_date', None):
            params['issuedAt[before]'] = self.config['end_date']
        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        result = response.json()
        data_result = result.get('hydra:member', None)
        yield from data_result

    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:
        """As needed, append or transform raw data to match expected structure.

        Args:
            row: Individual record in the stream.
            context: Stream partition or context dictionary.

        Returns:
            The resulting record dict, or `None` if the record should be excluded.
        """

        row.update({
            'company_id': self.config['company_id'],
            'connector_id': self.config['connector_id'],
        })

        return row